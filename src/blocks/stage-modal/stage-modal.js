const stageModal = () => {
  const modals = Array.from(document.querySelectorAll('.stage-modal'));
  modals.forEach((modal) => {
    const details = modal.querySelector('.stage-modal__details');
    const price = modal.querySelector('.stage-modal__price span');
    const radios = Array.from(modal.querySelectorAll('input[name="stage-form-type"]'));
    radios.forEach((radio) => {
      radio.addEventListener('input', () => {
        details.classList.add('stage-modal__details--visible');
        price.innerHTML = `${price.dataset.min * radio.dataset.costForSingleGraft}&nbsp;— ${price.dataset.max * radio.dataset.costForSingleGraft}`;
      });
    });
    modal.addEventListener('reset', () => {
      details.classList.remove('stage-modal__details--visible');
      price.innerHTML = '';
    });
  });
};

export default stageModal;
