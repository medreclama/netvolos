const stage = () => {
  const btns = Array.from(document.querySelectorAll('.stage__button a'));
  btns.forEach((btn) => {
    btn.addEventListener('click', (e) => {
      e.preventDefault();
      const form = document.getElementById(btn.href.substr(btn.href.indexOf('#') + 1));
      const html = document.querySelector('html');
      if (form) {
        const inner = form.querySelector('.stage-modal__inner');
        const price = form.querySelector('.stage-modal__price span');
        form.classList.add('stage-modal--visible');
        html.style.setProperty('overflow', 'hidden');
        if (form.elements.type.value !== btn.dataset.stage) {
          form.reset();
          if (e.target.dataset.graftsMaxQty > 0) {
            if (e.target.dataset.stage) form.elements.type.value = e.target.dataset.stage;
            if (e.target.dataset.id) form.elements.id.value = e.target.dataset.id;
            price.dataset.min = e.target.dataset.graftsMinQty;
            price.dataset.max = e.target.dataset.graftsMaxQty;
          } else {
            inner.classList.add('stage-modal__inner--no-calc');
          }
        }
        document.addEventListener('keydown', (ev) => {
          if (ev.code === 'Escape' && form.classList.contains('stage-modal--visible')) {
            ev.preventDefault();
            form.classList.remove('stage-modal--visible');
            html.style.removeProperty('overflow');
            inner.classList.remove('stage-modal__inner--no-calc');
          }
        });
        form.addEventListener('click', (ev) => {
          if (ev.target.classList.contains('stage-modal--visible') || ev.target.classList.contains('stage-modal__close')) {
            ev.currentTarget.classList.remove('stage-modal--visible');
            html.style.removeProperty('overflow');
            inner.classList.remove('stage-modal__inner--no-calc');
          }
        });
      }
    });
  });
};

export default stage;
