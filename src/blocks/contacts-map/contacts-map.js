const contactsMap = () => {
  const map = document.querySelector('.contacts-map__map-inner');

  if (map) {
    const resize = () => map.style.setProperty('width', `${map.offsetWidth + (document.body.clientWidth - map.getBoundingClientRect().left)}px`);
    resize();
    window.addEventListener('resize', () => requestAnimationFrame(resize));
  }
};

export default contactsMap;
