const homeContacts = () => {
  const map = document.querySelector('.home-contacts__map-inner');

  if (map) {
    const resize = () => map.style.setProperty('width', `${map.offsetWidth + (document.body.clientWidth - map.getBoundingClientRect().right)}px`);
    resize();
    window.addEventListener('resize', () => requestAnimationFrame(resize));
  }
};

export default homeContacts;
