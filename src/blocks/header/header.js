const header = () => {
  const h = document.querySelector('.header');
  const hTopPos = document.querySelector('.header__top').getBoundingClientRect().bottom + window.pageYOffset;
  const hHeight = h.offsetHeight;
  const pageBlock = document.querySelector('.page');
  pageBlock.style.setProperty('--page-margin-top', `${hHeight}px`);
  window.addEventListener('scroll', () => {
    if (window.pageYOffset >= hTopPos && !h.classList.contains('header--fixed')) {
      h.classList.add('header--fixed');
    } else if (window.pageYOffset < hTopPos) {
      h.classList.remove('header--fixed');
    }
  });
};

export default header;
