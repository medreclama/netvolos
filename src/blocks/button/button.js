const returnCenter = (elem, start, stopPos) => {
  const time = new Date().getTime();
  const pos = parseInt(getComputedStyle(elem).getPropertyValue('--btn-gradient-center'), 10);
  const newPos = pos - (((time - start) / 1000) * 60) / 0.4;
  elem.style.setProperty('--btn-gradient-center', `${newPos}px`);
  if (newPos >= stopPos) {
    window.requestAnimationFrame(() => {
      returnCenter(elem, start, stopPos);
    });
  } else {
    elem.style.removeProperty('--btn-gradient-center');
  }
};

export default function buttonHover() {
  const btns = Array.from(document.querySelectorAll('.btn'));

  btns.forEach((btn) => {
    const xPos = parseInt(getComputedStyle(btn).getPropertyValue('--btn-gradient-center'), 10);
    btn.addEventListener('mousemove', (e) => {
      e.target.style.setProperty('--btn-gradient-center', `${e.pageX - btn.getBoundingClientRect().left}px`);
    });
    btn.addEventListener('mouseleave', (e) => {
      const start = new Date().getTime();
      returnCenter(e.target, start, xPos);
    });
  });
}
