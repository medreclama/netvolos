import Timer from 'easytimer.js';

const toDigits = (num) => {
  const str = num.toString().length > 1 ? num.toString() : `0${num.toString()}`;
  return str.split('');
};

const setDigits = (item, countdown, pos) => {
  const i = item;
  i.innerHTML = `<div class="countdown__digit">${toDigits(countdown.getTimeValues()[pos])[0]}</div><div class="countdown__digit">${toDigits(countdown.getTimeValues()[pos])[1]}</div>`;
};

const countdown = () => {
  const countdowns = Array.from(document.querySelectorAll('.countdown'));
  if (countdowns) {
    countdowns.forEach((item) => {
      if (item.dataset.countdown) {
        const date = new Date(item.dataset.countdown);
        const t = new Timer();
        const positions = Array.from(item.querySelectorAll('.countdown__digits'));
        const cd = positions.map((pos) => {
          const mod = 'countdown__digits--';
          const className = Object.values(pos.classList)
            .filter((cName) => cName.includes(mod))[0];
          return { name: className.slice(mod.length), element: pos };
        });
        t.start({
          countdown: true,
          startValues: { seconds: (+date - +Date.now()) / 1000 },
        });
        cd.forEach((i) => {
          setDigits(i.element, t, i.name);
          t.addEventListener(`${i.name}Updated`, () => {
            setDigits(i.element, t, i.name);
          });
        });
      } else {
        item.remove();
      }
    });
  }
};

export default countdown;
