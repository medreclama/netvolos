const toLastDay = () => {
  const items = document.querySelectorAll('.to-last-day');
  const date = new Date();
  const month = date.getMonth();
  const months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноябрь', 'декабря'];
  const day = new Date(date.getFullYear(), month + 1, 0).getDate();
  if (!items) return;
  Array.from(items).forEach((item) => {
    item.replaceWith(`${day} ${months[month]}`);
  });
};

export default toLastDay;
