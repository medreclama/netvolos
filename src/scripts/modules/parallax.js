import iivp from './isInViewport';

const setTransform = (elem, property, start, offset) => {
  if (iivp(elem)) {
    const height = (window.innerHeight || document.documentElement.clientHeight);
    const blockTop = elem.getBoundingClientRect().top;
    const y = start + (offset * ((height - blockTop) / height));
    elem.style.setProperty(property, `${y}px`);
  }
};

const parallax = (obj, property, start, offset) => {
  if (obj) {
    const arr = Symbol.iterator in Object(obj) ? Array.from(obj) : [obj];

    arr.forEach((elem) => {
      setTransform(elem, property, start, offset);
    });

    window.addEventListener('scroll', () => requestAnimationFrame(() => {
      arr.forEach((elem) => {
        setTransform(elem, property, start, offset);
      });
    }));
  }
};

export default parallax;
