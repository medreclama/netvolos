import Splide from '@splidejs/splide';
import GLightbox from 'glightbox';
import likely from 'ilyabirman-likely';
import parallax from './modules/parallax';
import toLastDay from './modules/toLastDay';
import sh from '../blocks/show-hide/show-hide';
import tabs from '../blocks/tabs/tabs';
import buttonHover from '../blocks/button/button';
import header from '../blocks/header/header';
import headerMobileSwitcher from '../blocks/header-mobile-switcher/header-mobile-switcher';
import appear from '../blocks/appear/appear';
import homeContacts from '../blocks/home-contacts/home-contacts';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import countdown from '../blocks/countdown/countdown';
import stageModal from '../blocks/stage-modal/stage-modal';
import stage from '../blocks/stage/stage';
import modal from '../blocks/modal/modal';
import accordionItem from '../blocks/accordion-item/accordion-item';

likely.initiate();
toLastDay();
sh();
tabs();
buttonHover();
header();
headerMobileSwitcher();
homeContacts();
appear();
headerNavigation();
countdown();
stageModal();
stage();
modal();
accordionItem();

const lightbox = GLightbox({ // eslint-disable-line no-unused-vars
  selector: '[data-glightbox]',
  height: 'auto',
  draggable: false,
});

const stages = Array.from(document.querySelectorAll('.stages-slider'));

stages.forEach((slider) => {
  new Splide(slider, {
    perPage: 3,
    rewind: false,
    perMove: 1,
    pagination: false,
    start: 0,
    autoWidth: true,
    breakpoints: {
      816: {
        perPage: 2,
      },
      479: {
        perPage: 1,
      },
    },
  }).mount();
});

const results = Array.from(document.querySelectorAll('.results'));

results.forEach((slider) => {
  new Splide(slider, {
    perPage: 2,
    rewind: false,
    perMove: 1,
    pagination: false,
    start: 0,
    autoWidth: true,
    breakpoints: {
      816: {
        perPage: 1,
      },
    },
  }).mount();
});

parallax(document.querySelectorAll('.list-advantages__item'),
  '--icon-top',
  -8,
  12);
parallax(document.querySelectorAll('.page-section--background'),
  '--bg-translate',
  120,
  -240);

parallax(document.querySelector('.home-doctor__video'),
  '--translate-y',
  (window.innerHeight / 20),
  -(window.innerHeight / 20) - 10);

window.addEventListener('contextmenu', (e) => e.preventDefault());
window.addEventListener('copy', (e) => e.preventDefault());
